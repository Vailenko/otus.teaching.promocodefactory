﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        public Task DeleteAsync(Guid id)
        {
            var item = Data.FirstOrDefault(x => x.Id == id);
            ((List<T>)Data).Remove(item);
            return Task.CompletedTask;
        }
        public Task CreateAsync(T entity)
        {            
            ((List<T>)Data).Add(entity);            
            entity.Id = Guid.NewGuid();
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {
            var item = ((List<T>)Data).FirstOrDefault(x => x.Id == entity.Id);
            ((List<T>)Data).Remove(item);
            ((List<T>)Data).Add(entity);    
            return Task.CompletedTask;
        }
    }
}